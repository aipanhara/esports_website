Rails.application.routes.draw do

    devise_for :users, controllers: {
      sessions: 'users/sessions',
      registrations: "users/registrations"
    }

    # devise_for :admins, :controllers => { :registrations => :registrations }
    devise_for :admins, :skip => [:registrations]

    devise_scope :admin do
      get "/signin" => "devise/sessions#new", :as => "signin" # custom path to login/sign_in
    end

    devise_scope :user do
      get "/sign_in" => "devise/sessions#new", :as => "sign_in" # custom path to login/sign_in
      get "/sign_up" => "devise/registrations#new", :as => "sign_up" # custom path to login/sign_in
      get "users/verify/" => 'users/registrations#show_verify', as: 'verify'
      post "users/verify"
      post "users/resend"
    end

    as :admin do
      get 'admins/edit' => 'devise/registrations#edit', :as => 'edit_admin_registration'
      put 'admins' => 'devise/registrations#update', :as => 'admin_registration'
      patch 'admins' => 'devise/registrations#update', :as => 'update_admin_registration'
    end

    namespace :admin do
      root "dashboards#index"
      get 'dashboards' => "dashboards#index", :as => "dashboard"
      get 'invoices/:id' => 'orders#invoice', as: "invoice"

      resources :users
      resources :admins
      resources :categories
      resources :blogs
      resources :settings, only: [:index, :new, :update, :create, :edit]
    end

    get 'under_construction' => "static_pages#under_construction", as: "under_construction"
    root 'static_pages#index'
    resources :blogs, only: [:index, :show]
    resources :categories, only: [:index, :show]
    resources :tags, only: [:show]
end
