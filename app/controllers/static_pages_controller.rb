class StaticPagesController < FrontEndApplication
  before_action :settings?, only: [:index, :about, :contact, :term, :faq, :privacy, :guide, :delivery]

  def index
    @slide_shows = Blog.is_slide_active?
    @big_risings = Blog.is_order_limit_one?
    @other_risings = Blog.not_show_first_record
    @trendings = Blog.joins(:category).where("category_name = ?", "Trending")
    @populars = Blog.all.order("created_at desc").limit(6)
    @editor_picks = Blog.all.order(:created_at).limit(3)
    @categories = Category.all
  end

  def under_construction

  end

  private
    def settings?
      if Maintain.exists?
        if Maintain.first.under_maintain == true
          if Maintain.first.option == params[:controller]
            redirect_to under_construction_path
          end
        end
      end
    end
end
