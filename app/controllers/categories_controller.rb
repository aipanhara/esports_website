class CategoriesController < FrontEndApplication
  def index

  end

  def show
    @populars = Blog.all.order("created_at desc").limit(6)
    @category  = Category.find_by_slug(params[:id])
    @categories = Category.all
    @other_risings = Blog.not_show_first_record
    @editor_picks = Blog.all.order(:created_at).limit(3)
  end
end
