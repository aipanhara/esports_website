class TagsController < FrontEndApplication
  def show
    @categories = Category.all
    @editor_picks = Blog.all.order(:created_at).limit(3)
    @other_risings = Blog.not_show_first_record
    @tag = Tag.find_by_slug(params[:id])
    @tags = Tag.where(name: params[:id])
  end
end
