class BlogsController < FrontEndApplication
  def index
    @categories = Category.all
    @editor_picks = Blog.all.order(:created_at).limit(3)
    @other_risings = Blog.not_show_first_record
    @populars = Blog.all.order("created_at desc").page(params[:page]).per(4)
    @blogs = Blog.all
  end

  def show
    @categories = Category.all
    @editor_picks = Blog.all.order(:created_at).limit(3)
    @blog = Blog.find_by_slug(params[:id])
  end
end
