// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require_tree ./front-end/js
//= require social-share-button

$(document).ready(function(){
  var em = document.createElement("em");
  var i = document.createElement("i");
  var span = document.createElement("span");
  var inext = document.createElement("i");
  $("div.social-share-button").attr('id',"fb");
  $("div.social-share-button").addClass("tg-usericonholder");
  $(".ssb-facebook").append(em);
  $("em").addClass("tg-usericonholder");
  $("em").append(i);
  $("i").addClass("fa fa-facebook-f");
  $("em").append(span);
  $("span").addClass("s-fb");
  $("#view-post").removeClass("s-fb");
  $("div.social-share-button").removeClass("social-share-button");
  $(".lnr").removeClass("fa fa-facebook-f");
  $(".fa").removeClass("fa-facebook-f");
  $("#btn-next").text("");
  $("#btn-next").append(inext);
  $("#btn-next").addClass("fa fa-angle-right");
  $("#btn-last").text("");
  $("#btn-last").append(inext);
  $("#btn-last").addClass("fa fa-angle-double-right");
  $("#first-page").text("");
  $("#first-page").append(inext);
  $("#first-page").addClass("fa fa-angle-left");
  $("#last-page").text("");
  $("#last-page").append(inext);
  $("#last-page").addClass("fa fa-angle-double-left");
});
