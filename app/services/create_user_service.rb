class CreateUserService
  def call
    user = User.find_or_create_by!(email: Rails.application.secrets.user_email) do |user|
        user.phone_number = Rails.application.secrets.phone_number
        user.password = Rails.application.secrets.user_password
        user.password_confirmation = Rails.application.secrets.user_password
        user.customer!
      end
  end
end
