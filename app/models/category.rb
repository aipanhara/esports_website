class Category < ApplicationRecord
  before_save :update_slug

  has_many :blogs
  belongs_to :admin

  def update_slug
    self.slug = category_name.parameterize
  end

  def to_param
    slug
  end
end
