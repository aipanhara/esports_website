class Blog < ApplicationRecord
  before_save :update_slug

  belongs_to :admin
  belongs_to :category
  has_many :tags, inverse_of: :blog
  accepts_nested_attributes_for :tags, :reject_if => :all_blank, :allow_destroy => true

  has_attached_file :image, styles: {
    medium: "470x500#",
    thumb: "80x80#",
    popular: "270x370#",
    trending: "270x300#",
    daily: "384x245#",
    explore: "270x200#",
    category: "205x160#"
  }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def update_slug
    self.slug = title.parameterize
  end

  def to_param
    slug
  end

  def self.is_slide_active?
    where('is_slide = true')
  end

  def self.is_order_limit_one?
    order("created_at desc").limit(1)
  end

  def self.not_show_first_record
    where.not('id = 1').order('created_at desc').limit(8)
  end

end
