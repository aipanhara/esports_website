class Tag < ApplicationRecord
  before_save :update_slug
  belongs_to :blog

  def update_slug
    self.slug = name.parameterize
  end

  def to_param
    slug
  end
end
