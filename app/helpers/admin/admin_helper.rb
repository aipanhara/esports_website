module Admin::AdminHelper
  def custom_flash_message
    flash_messages = []
    flash.each do |type, message|
      type = 'success' if type == 'notice'
      type = 'error' if type == 'alert'
      text = "<script>toastr.#{type}('#{message}');</script>"
      flash_messages << text.html_safe if message
    end
    flash_messages.join("\n").html_safe

  end

  def status_converter(status, correct: 'active', incorrect: 'inactive')
    if status
     correct
    else
      incorrect
    end
  end

  def time_ago(time)
    "#{time_ago_in_words(time)} ago"
  end

  def title(blog_title)
    content_for(:title) {blog_title}
  end

  def meta_description(blog_text)
    content_for(:meta_description) {blog_text}
  end
end
