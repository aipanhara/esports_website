class AddIsSlideToBlogs < ActiveRecord::Migration[5.1]
  def change
    add_column :blogs, :is_slide, :boolean
  end
end
