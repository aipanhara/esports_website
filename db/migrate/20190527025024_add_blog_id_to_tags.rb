class AddBlogIdToTags < ActiveRecord::Migration[5.1]
  def change
    add_column :tags, :blog_id, :integer
  end
end
