class CreateBlogs < ActiveRecord::Migration[5.1]
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :description
      t.string :author
      t.integer :category_id
      t.date :post_date
      t.integer :user_id

      t.timestamps
    end
  end
end
